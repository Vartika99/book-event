﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    [Table("Users")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private int id;
        [Required(AllowEmptyStrings =false)]
        [DataType(DataType.Text)]
        private String fullName;
        [Key]
        [Required(AllowEmptyStrings =false)]
        [DataType(DataType.Text)]
        [StringLength(60)]
        [Index(IsUnique = true)]
        public String Email { get; set; }
        [Required(AllowEmptyStrings =false)]
        [DataType(DataType.Password)]
        private String password;
        [Required(AllowEmptyStrings =false)]
        [DataType(DataType.Text)]
        private String role;

       
      //  public virtual ICollection<BookModel> model { get; set; }
        public User(string fullName, string email, string password)
        {
            this.fullName = fullName;
            this.Email = email;
            this.password = password;
            role = "User";
        }
        public User()
        {

        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public String FullName
        {
            get { return fullName; }
            set { fullName = value; }
        }
       
        public String Password
        {
            get { return password; }
            set { password = value; }
        }
        public String Role
        {
            get { return role; }
            set { role = value; }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccess.Models
{
    [Table("BookInfo")]
    public class BookModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }
       
        [Required(AllowEmptyStrings =false)]
        [DataType(DataType.Text)]
        [Column(Order = 2)]
        public string UserEmail { get; set; }
        [ForeignKey("UserEmail")]
        public virtual User Users { get; set; }
       
        [Required]
        [DataType(DataType.Text)]
        [Column(Order = 3)]
        public string Title { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        [Column(Order = 4)]
        public DateTime Date { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [Column(Order = 5)]
        public string Location { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [Column(Order = 6)]
        public string StartTime { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [Column(Order = 7)]
        public string Type { get; set; }
        [DataType(DataType.Text)]
        [Column(Order = 8)]
        public int Duration { get; set; }
        [DataType(DataType.Text)]
        [StringLength(50)]
        [Column(Order = 9)]
        public string Description { get; set; }
        [DataType(DataType.Text)]
        [StringLength(500)]
        [Column(Order = 10)]
        public string OtherDetails { get; set; }
        [DataType(DataType.Text)]
        [Column(Order = 11)]
        public string InviteByEmail { get; set; }
        public BookModel(string userEmail,string title, DateTime date, string location, string startTime, string type, int duration=0, string description=null, string otherDetails=null,string inviteByEmail=null)
        {
            UserEmail = userEmail;
            Title = title;
            Date = date;
            Location = location;
            StartTime = startTime;
            Type = type;
            Duration = duration;
            Description = description;
            OtherDetails = otherDetails;
            InviteByEmail = inviteByEmail;
        }
        public BookModel()
        {

        }
    }
}

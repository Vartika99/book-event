﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
     public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }

        [Column(Order = 2)]
        public int eventId { get; set; }
        [ForeignKey("eventId")]
        public virtual BookModel BookModels { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Text)]
        [Column(Order = 3)]
        public String commentBy { get; set; }
        [ForeignKey("commentBy")]
        public virtual User Users { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Column(Order = 4)]
        public string comment { get; set; }

        public Comment(int eventId, string commentBy, string comment)
        {
            this.eventId = eventId;
            this.commentBy = commentBy;
            this.comment = comment;
        }
        public Comment()
        {

        }
    }
}

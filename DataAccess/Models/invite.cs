﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    [Table("Invite")]
    public class Invite
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public int Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Text)]
        [Column(Order = 2)]
        [Index("IX_FirstAndSecond", 1, IsUnique = true)]
        public String email { get; set; }
        [ForeignKey("email")]
        public virtual User Users { get; set; }  
        [Column(Order = 3)]
        [Index("IX_FirstAndSecond", 2, IsUnique = true)]
        public int modelId { get; set; }
        [ForeignKey("modelId")]
        public virtual BookModel BookModels { get; set; }

        public Invite(string email, int modelId)
        {
            this.email = email;
            this.modelId = modelId;
        }
        public Invite()
        { }
    }
}

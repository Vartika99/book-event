﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditTable2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BookInfo", "Userid", "dbo.Users");
            DropIndex("dbo.BookInfo", new[] { "Userid" });
            RenameColumn(table: "dbo.BookInfo", name: "Userid", newName: "UserEmail");
            DropPrimaryKey("dbo.Users");
            AlterColumn("dbo.BookInfo", "UserEmail", c => c.String(nullable: false, maxLength: 60));
            AlterColumn("dbo.Users", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Users", "Email");
            CreateIndex("dbo.BookInfo", "UserEmail");
            AddForeignKey("dbo.BookInfo", "UserEmail", "dbo.Users", "Email", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookInfo", "UserEmail", "dbo.Users");
            DropIndex("dbo.BookInfo", new[] { "UserEmail" });
            DropPrimaryKey("dbo.Users");
            AlterColumn("dbo.Users", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.BookInfo", "UserEmail", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Users", "Id");
            RenameColumn(table: "dbo.BookInfo", name: "UserEmail", newName: "Userid");
            CreateIndex("dbo.BookInfo", "Userid");
            AddForeignKey("dbo.BookInfo", "Userid", "dbo.Users", "Id", cascadeDelete: true);
        }
    }
}

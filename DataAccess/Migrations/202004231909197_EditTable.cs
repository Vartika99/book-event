﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditTable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Users", new[] { "Email" });
            AddColumn("dbo.BookInfo", "Userid", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "Email", c => c.String(nullable: false, maxLength: 60));
            CreateIndex("dbo.BookInfo", "Userid");
            CreateIndex("dbo.Users", "Email", unique: true);
            AddForeignKey("dbo.BookInfo", "Userid", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookInfo", "Userid", "dbo.Users");
            DropIndex("dbo.Users", new[] { "Email" });
            DropIndex("dbo.BookInfo", new[] { "Userid" });
            AlterColumn("dbo.Users", "Email", c => c.String());
            DropColumn("dbo.BookInfo", "Userid");
            CreateIndex("dbo.Users", "Email", unique: true);
        }
    }
}

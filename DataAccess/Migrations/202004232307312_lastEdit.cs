﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lastEdit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        eventId = c.Int(nullable: false),
                        commentBy = c.String(nullable: false, maxLength: 60),
                        comment = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BookInfo", t => t.eventId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.commentBy, cascadeDelete: false)
                .Index(t => t.eventId)
                .Index(t => t.commentBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comment", "commentBy", "dbo.Users");
            DropForeignKey("dbo.Comment", "eventId", "dbo.BookInfo");
            DropIndex("dbo.Comment", new[] { "commentBy" });
            DropIndex("dbo.Comment", new[] { "eventId" });
            DropTable("dbo.Comment");
        }
    }
}

﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditTable3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invite",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        email = c.String(nullable: false, maxLength: 60),
                        modelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BookInfo", t => t.modelId, cascadeDelete:false )
                .ForeignKey("dbo.Users", t => t.email, cascadeDelete: false)
                .Index(t => new { t.email, t.modelId }, unique: true, name: "IX_FirstAndSecond");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invite", "email", "dbo.Users");
            DropForeignKey("dbo.Invite", "modelId", "dbo.BookInfo");
            DropIndex("dbo.Invite", "IX_FirstAndSecond");
            DropTable("dbo.Invite");
        }
    }
}

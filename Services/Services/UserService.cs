﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Models;
using DataAccess.AccessDatabase;
using Services.Services.DTO;


namespace Services.Services
{
    public class UserService
    {
        

        //  UserAccess userAccess;
        public bool SignUp(String FullName, String Email, String Password)
        {
            System.Diagnostics.Debug.WriteLine("SignUpMethod");

            User user = new User(FullName,Email,Password);
            System.Diagnostics.Debug.WriteLine("Usr");
            UserDataAccess userAccess = new UserDataAccess();
            userAccess.Users.Add(user);
            System.Diagnostics.Debug.WriteLine("addMethod");
            userAccess.SaveChanges();
           System.Diagnostics.Debug.WriteLine(userAccess.Users.First());
            System.Diagnostics.Debug.WriteLine("SaveChanges");
            return true;
        }

        public bool login(String email, String password)
        {
            UserDataAccess userAccess = new UserDataAccess();
            //List<string> l = new List<string> { "sbc" };
            bool IsValid = userAccess.Users.Any(x => x.Email == email && x.Password==password);
            System.Diagnostics.Debug.WriteLine(email+" "+IsValid);

          //  foreach (var val in userdata)
          //  System.Diagnostics.Debug.WriteLine(userdata.Email);
            //  Dictionary<string,Object> list = new Dictionary<string,object> { "data":userdata; "token":token };
          //  UserInfo userInfo = new UserInfo();
            //userInfo.user = userdata;
           // userInfo.token = token;
            return IsValid ;
        }


        //Create Event 
        public List<String> CreateEvent(string identity,string title, DateTime date, string location, string datetime, string type, int duration, string description, string details, string inviteByEmail)
        {
            UserDataAccess userAccess = new UserDataAccess();

            List<String> ValidEmails = new List<string>();
            List<String> InvalidEmails = new List<string>();
            if ((inviteByEmail!=null)&&(inviteByEmail.Trim().Length != 0))
            {
                String[] Emails = inviteByEmail.Split(',').Distinct().ToArray();
                inviteByEmail = "";

                foreach (string email in Emails)
                {
                    if (userAccess.Users.Any(x => x.Email == email.Trim()))
                    {
                        inviteByEmail += ("," + email.Trim());
                        ValidEmails.Add(email.Trim());
                    }
                    else
                    {
                        InvalidEmails.Add(email.Trim());
                        
                    }
                }

            }
            BookModel bookModel = new BookModel(identity, title, date, location, datetime, type,duration,description,details,inviteByEmail);
            userAccess.BookModels.Add(bookModel);
            System.Diagnostics.Debug.WriteLine("addMethod");
            userAccess.SaveChanges();
            int modelId = bookModel.Id;
            foreach(string email in ValidEmails)
            {
                Invite invite = new Invite(email, modelId);
                userAccess.Invites.Add(invite);
                userAccess.SaveChanges();
            }
            return InvalidEmails;
        }
        public IQueryable<HomePageInfo> showBookEvents()
        {
            System.Diagnostics.Debug.WriteLine("startdetails");
            UserDataAccess userAccess = new UserDataAccess();
          IQueryable<HomePageInfo> homepagedata = userAccess.BookModels.Select(x => new HomePageInfo() {Id=x.Id,Title=x.Title });
            return homepagedata;
        }

        public IQueryable<BookModel> GetBookInfo(int ID)
        {
            UserDataAccess userAccess = new UserDataAccess();
            IQueryable<BookModel> bookInfos = userAccess.BookModels.Where(x => x.Id == ID);
            return bookInfos;
        }

        public IQueryable<Invite> GetInvite(string identity,int id)
        {
            UserDataAccess userDataAccess = new UserDataAccess();
            IQueryable<Invite> inviteInfos = userDataAccess.Invites.Where(x => x.email == identity&&x.modelId==id);
                return inviteInfos;
        }

        public bool addComment(string Identity, int eventId, string comment)
        {
            try
            {
                UserDataAccess userDataAccess = new UserDataAccess();
                Comment commentmodel = new Comment(eventId, Identity, comment);
                userDataAccess.Comments.Add(commentmodel);
                userDataAccess.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }
        public List<IQueryable<BookModel>> myEvent(string Identity)
        {
            
            UserDataAccess userDataAccess = new UserDataAccess();
            IQueryable<BookModel> pastbookModels= userDataAccess.BookModels.Where(x => x.UserEmail == Identity&& x.Date< DateTime.Today);
            IQueryable<BookModel> futurebookModels = userDataAccess.BookModels.Where(x => x.UserEmail == Identity && x.Date >= DateTime.Today);
            List<IQueryable<BookModel>> output = new List<IQueryable<BookModel>>() { pastbookModels, futurebookModels };
            return output;
        }
    }
}

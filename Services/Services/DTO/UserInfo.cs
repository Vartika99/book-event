﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services.DTO
{
    public class UserInfo
    {
        public User user { get; set; }
        public long token { get; set; }
    }
}

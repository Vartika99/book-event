﻿
using BookEventsAPI.Models;
using Services.Services;
using Services.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

namespace BookEventsAPI.Controllers
{
    
    public class UserController : Controller
    {
        // GET: User

        public ActionResult Index()
        {
            System.Diagnostics.Debug.WriteLine("indexed");
            return View();
        }
        public ActionResult LogIn()
        {
            return View("Login");
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(User user)
        {
            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                {
                    return View();
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Entered");

                    UserService userService = new UserService();
                    if (userService.SignUp(user.FullName, user.Email, user.Password))
                    { 
                        System.Diagnostics.Debug.WriteLine("finished");
                    return View();
                     }


                }
                return RedirectToAction("SignUpForm");
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult Login(Login login)
        {
            System.Diagnostics.Debug.WriteLine("ENTER done");
            UserService userService = new UserService();
            if(userService.login(login.authemail, login.authpassword))
            {
                FormsAuthentication.SetAuthCookie(login.authemail, false);
                System.Diagnostics.Debug.WriteLine("ENTER  okay");
                
                return View("../Home/Index");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error");
                
                ViewBag.Message = "Invalid UserName and Password";
             // ModelState.AddModelError("", "Invalid UserName and Password");
            }
  
            return View();
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

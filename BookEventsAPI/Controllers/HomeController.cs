﻿using BookEventsAPI.Models;
using DataAccess.Models;
using Newtonsoft.Json;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace BookEventsAPI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CreateEvent()
        {
            return View("AddEvent");
        }
        [HttpPost]
        public ActionResult Event(CreateEvent createEvent)
        {
            System.Diagnostics.Debug.WriteLine("ENTER done");
            UserService userService = new UserService();
            List<String> invalidemails = userService.CreateEvent(HttpContext.User.Identity.Name, createEvent.title, createEvent.date, createEvent.location, createEvent.datetime, createEvent.type, createEvent.duration, createEvent.description, createEvent.details, createEvent.inviteByEmail);
            ViewBag.Message = invalidemails.ToString();
            return View("AddEvent");
        }
        [HttpPost]
        public ActionResult Add(CreateComment createComment)
        {
            System.Diagnostics.Debug.WriteLine("ENTER done");
            UserService userService = new UserService();
            if (userService.addComment(HttpContext.User.Identity.Name, createComment.Id, createComment.comment))
            {
                return View("Home");
            }
            else
            {
                ViewBag.Message = "Error in Adding Comment Please Try Again";
                return View();
            }
        }
        [HttpGet]
        public string BookInfo(int id)
        {
            UserService userService = new UserService();
            IQueryable<BookModel> books= userService.GetBookInfo(id);
            string output = JsonConvert.SerializeObject(new
            {
                books
            });
            return output;
        }
        [HttpGet]
    public string EventInvitedTo(int id)
    {
            UserService userService = new UserService();
            IQueryable<Invite> invites   =userService.GetInvite(HttpContext.User.Identity.Name,id);
            string output = JsonConvert.SerializeObject(new
            {
                    invites
            });
            return output;
    }
        [HttpGet]
        public string MyEvent()
        {
            UserService userService = new UserService();
            List<IQueryable<BookModel>> bookinfo = userService.myEvent(HttpContext.User.Identity.Name);
            string output = JsonConvert.SerializeObject(new
            {
                bookinfo
             });
            return output;
        }
    }

}

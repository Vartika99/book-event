﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookEventsAPI.Models
{
    public class CreateComment
    {
        public int Id { get; set; }
        public string comment { get; set; }
    }
}
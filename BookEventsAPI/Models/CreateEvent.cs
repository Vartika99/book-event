﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookEventsAPI.Models
{
    public class CreateEvent
    {
       // [Display(Name = "Name")]
      //  [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public string title { get; set; }
        //  [Display(Name = "Date")]
        //  [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public DateTime date { get; set; }
        //  [Display(Name = "Location")]
        //  [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public string location { get; set; }
        //  [Display(Name = "StartTime")]
        // [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public string datetime { get; set; }
        // [Display(Name = "Type")]
        // [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public string type { get; set; }
        // [Display(Name = "Duration")]
        // [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public int duration { get; set; }
        //[Display(Name = "Description")]
        //  [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public string description { get; set; }
        //  [Display(Name = "OtherDetails")]
        //  [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public string details { get; set; }
        public string inviteByEmail { get; set; }

        
    }
}
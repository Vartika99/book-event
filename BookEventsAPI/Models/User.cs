﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookEventsAPI.Models
{
    public class User
    {
        [Display(Name = "Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        public string FullName { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email ID is not in correct format")]
        public string Email { get; set; }
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Required")]
        // [RegularExpression(@"^(?=.*[a - z])(?=.*[A - Z])(?=.*[0 - 9])(?=.*[!@#\$%\^&\*])(?=.{8,})$", ErrorMessage = "Password Must Contain 1 uppercase, 1 lowercase, 1 digit, 1 special character and length should be longer than eight ")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}

